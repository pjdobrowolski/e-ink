import requests

resp = requests.get('http://worldtimeapi.org/api/ip').json()
h = resp['datetime'][resp['datetime'].find('T')+1:resp['datetime'].find('T')+3]
m = resp['datetime'][resp['datetime'].find('T')+4:resp['datetime'].find('T')+6]
print(h , m)

resp = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Elblag,PL&APPID=0134d7827a499d9b173801821f665596').json()
print(resp['weather'][0]['icon']) #to decode it please see page listed below
print('actual_temp is', str(int(resp['main']['temp'])-273)+chr(176)+'C') # 273.15 is conversion rate for C to K, icons avaible at https://openweathermap.org/weather-conditions
