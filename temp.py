from PIL import ImageFont, ImageDraw, Image

image = Image.new('RGBA', (400,187), color = (255,255,255,255))

draw = ImageDraw.Draw(image)

# use a bitmap font
font = ImageFont.truetype("graphics/fonts/Strawberry Muffins Demo.ttf", 150)

draw.text((10, (187-150)/2), "world", fill=(0,0,0), font=font)

image.show()
