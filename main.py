from screen_proc import *
import requests
import json

print("Loading world time")

resp = requests.get('http://worldtimeapi.org/api/ip').json()
h = resp['datetime'][resp['datetime'].find('T')+1:resp['datetime'].find('T')+3]
m = resp['datetime'][resp['datetime'].find('T')+4:resp['datetime'].find('T')+6]

with open('/home/pi/weather_api.json') as json_data:
	API_key = json.load(json_data)

print("Loading local weather")
print(API_key["API_key"])
resp = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Elblag,PL&APPID='+API_key["API_key"]).json()
weather = resp['weather'][0]['icon']
temperature = str(int(resp['main']['temp'])-273)

print("Screen init")
weather_clock = Screen()

print("Time update")
weather_clock.time_digits(h, m)

print("Weather update")
weather_clock.weather_status(temperature, weather)

print("Screen update")
weather_clock.update()
