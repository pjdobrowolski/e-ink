import RPi.GPIO as GPIO
import json
import subprocess
from time import sleep

# https://raspberrypihq.com/use-a-push-button-with-raspberry-pi-gpio/
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 10 to be an input pin and set initial value to be pulled low (off)

with open('/home/pi/e-ink/radios.json') as json_data:
    config = json.load(json_data)

radios = config["radio"][0]
poz = 0

index = []
for key in radios:
	index.append(key)

while(1):
	try:
		GPIO.wait_for_edge(10, GPIO.RISING)
		print(radios[index[poz]])
		myprocess = subprocess.Popen(['tmux','kill-session','-t','session'])
		sleep(0.1)
		myprocess = subprocess.Popen(['tmux','new-session','-d','-s','session','omxplayer',radios[index[poz]]])
		poz+=1
		if poz >= len(index):
			poz=0
	except KeyboardInterrupt:  
		GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
	 
GPIO.cleanup() # Clean up

