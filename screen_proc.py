from PIL import Image, ImageFont, ImageDraw
import json
from waveshare_epd import epd4in2


with open('/home/pi/e-ink/config.json') as json_data:
	config = json.load(json_data)

################################################
#	coords of image starts from top left corner
#   scree has 400x300px
#
#
#
#
#
#
#
#
################################################


class Screen:
	def __init__(self):
		self.clock = []
		self.temp = []

	def time_digits(self,hour,minutes):
		fonts = [config["path"], config["name"], config["format"], config["digits_outline"]]

		h0 = Image.open(fonts[0] + fonts[1] + hour[0] + fonts[2])
		h1 = Image.open(fonts[0] + fonts[1] + hour[1] + fonts[2])
		m0 = Image.open(fonts[0] + fonts[1] + minutes[0] + fonts[2])
		m1 = Image.open(fonts[0] + fonts[1] + minutes[1] + fonts[2])
		spacer = Image.open(fonts[0] + fonts[1] + ":" + fonts[2])

		self.clock = Image.new('RGBA', (4 * h0.size[0] + spacer.size[0] + fonts[3]*6, h0.size[1]+2*fonts[3]), color = (255, 255, 255, 255))
		mask = Image.new('RGBA', self.clock.size, color = (255,255,255,255))
		y_pos_clock = fonts[3] + 8
		self.clock.paste(h0,(fonts[3],y_pos_clock))
		self.clock.paste(h1,(2*fonts[3]+h0.size[0],y_pos_clock))
		self.clock.paste(spacer,(3*fonts[3]+2*h0.size[0],y_pos_clock))
		self.clock.paste(m0,(4*fonts[3]+2*h0.size[0]+spacer.size[0],y_pos_clock))
		self.clock.paste(m1,(5*fonts[3]+3*h0.size[0]+spacer.size[0],y_pos_clock))
		self.clock = Image.alpha_composite(mask,self.clock)

	def weather_status(self, temperature, condition):
		# 400 x 187
		temperature_graphics = config["Temp_symbol"],config["Temp_font"]

		self.temp = Image.new('RGBA', (400 ,187), color = (255,255,255,255))

		draw = ImageDraw.Draw(self.temp)

		font = ImageFont.truetype(temperature_graphics[1], 100)

		draw.text((9, (187-80)//2), temperature, fill=(0,0,0), font=font)
		temp_symbol = Image.open(temperature_graphics[0])
		mask = Image.new('RGBA', temp_symbol.size, color=(255, 255, 255, 255))
		temp_symbol = Image.alpha_composite(mask, temp_symbol)

		self.temp.paste(temp_symbol, (120, 60))
		# start x position if from 220px
		condition_symbol = Image.open(config["path"]+config["Weather_status"][0][condition])
		mask = Image.new('RGBA', condition_symbol.size, color=(255, 255, 255, 255))
		condition_symbol = Image.alpha_composite(mask, condition_symbol)

		self.temp.paste(condition_symbol, (240, 20))

	def update(self):
		epd = epd4in2.EPD()
		epd.init()

		screen = Image.new('RGBA', (400,300), color=(255, 255, 255, 255))
		screen.paste(self.clock, (0, 0))
		screen.paste(self.temp, (0,300-187))
#		screen.show()
#		redimage = Image.new('1',(epd.width, epd.height), 255)
#		epd.display(epd.getbuffer(screen), epd.getbuffer((redimage)))
#		epd.EPD_4IN2_PartialDisplay(self, X_start, Y_start, X_end, Y_end, Image):
		epd.Clear()
		epd.display(epd.getbuffer(screen))
		epd.sleep()
		epd4in2.epdconfig.module_exit()
	
	def update_clock(self):
		epd = epd4in2.EPD()
		epd.init()
		screen = Image.new('RGBA', (400,300-187), color=(255, 255, 255, 255))
		screen.paste(self.clock, (0, 0))
		epd.EPD_4IN2_PartialDisplay(0, 0, 400, 300-187, screen)
		epd.sleep()
		epd4in2.epdconfig.module_exit()

